from bonus_system import calculateBonuses
import random

# for "Standard" program

def test_standart_low():
    amount = random.randint(0,10000)
    assert round(calculateBonuses("Standard", amount), 2) == 0.5
    
def test_standart_medium():
    amount = random.randint(10000, 50000)
    assert round(calculateBonuses("Standard", amount), 2) == 0.75

def test_standart_high():
    amount = random.randint(50000, 100000)
    assert round(calculateBonuses("Standard", amount), 2) == 1

def test_standart_max():
    amount = random.randint(100000, 1000000)
    assert round(calculateBonuses("Standard", amount), 2) == 1.25

# for "Premium" program

def test_premium_low():
    amount = random.randint(0,10000)
    assert round(calculateBonuses("Premium", amount), 2) == 0.1
    
def test_premium_medium():
    amount = random.randint(10000, 50000)
    assert round(calculateBonuses("Premium", amount), 2) == 0.15

def test_premium_high():
    amount = random.randint(50000, 100000)
    assert round(calculateBonuses("Premium", amount), 2) == 0.2

def test_premium_max():
    amount = random.randint(100000, 1000000)
    assert round(calculateBonuses("Premium", amount), 2) == 0.25

# for "Diamond" program

def test_diamond_low():
    amount = random.randint(0,10000)
    assert round(calculateBonuses("Diamond", amount), 2) == 0.2
    
def test_diamond_medium():
    amount = random.randint(10000, 50000)
    assert round(calculateBonuses("Diamond", amount), 2) == 0.3

def test_diamond_high():
    amount = random.randint(50000, 100000)
    assert round(calculateBonuses("Diamond", amount), 2) == 0.4

def test_diamond_max():
    amount = random.randint(100000, 1000000)
    assert round(calculateBonuses("Diamond", amount), 2) == 0.5

# no gt or lt in string comparison

def test_wrong_naming():
    assert calculateBonuses("A", 10000) == 0
    assert calculateBonuses("Z", 10000) == 0

# check minimal boundaries

def test_boundaries():
    assert round(calculateBonuses("Standard", 10000), 2) == 0.75
    assert round(calculateBonuses("Standard", 50000), 2) == 1
    assert round(calculateBonuses("Standard", 100000), 2) == 1.25
