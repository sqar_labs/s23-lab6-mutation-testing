from calculator import count
import random

# test for equality of operands
# thanks to the QUALITY code we need to try-catch exception caused by absence of valid operand

def test_wrong_operands_lt():
    try:
        print(count(1, 1, '(')) # chr(40)
    except:
        assert(True)
        return
    assert(False)

def test_wrong_operands_gt():
    try:
        print(count(1, 1, '1')) # chr(49) - all operands are within the range 40-49
    except:
        assert(True)
        return
    assert(False)

# test for devision by neg. value (alongside with test_valid_div_negative_float)
def test_valid_div_negative_divider():
    a = 1
    b = -1
    assert count(a,b,'/') == -1

# tests from the lab

def test_valid_sum():
    a = random.randint(0,100000)
    b = random.randint(0,100000)
    assert count(a,b, '+') == (a+b)

def test_valid_sum_float():
    a = 2.5
    b = 11.32
    assert count(a,b,'+') == (2.5+11.32)

def test_valid_sum_negative_test():
    a = 2.5
    b = -11.32
    assert count(a,b,'+') == (2.5+(-11.32))

def test_valid_sub():
    a = random.randint(0,100000)
    b = random.randint(0,100000)
    assert count(a,b, '-') == (a-b)

def test_valid_sub_float():
    a = 7.3
    b = 2.1
    assert count(a,b,'-') == (7.3-2.1)

def test_valid_sub_negative_float():
    a = 7.3
    b = 11.1
    assert count(a,b,'-') == (7.3-11.1)

def test_valid_div():
    a = random.randint(0,100000)
    b = random.randint(0,100000)
    assert count(a,b, '/') == (a/b)

def test_valid_div_float():
    a = 22
    b = 33
    assert count(a,b,'/') == (22/33)

def test_valid_div_negative_float():
    a = -22
    b = 33
    assert count(a,b,'/') == (-22/33)

def test_valid_mult():
    a = random.randint(0,100000)
    b = random.randint(0,100000)
    assert count(a,b, '*') == (a*b)

def test_valid_mult_float():
    a = 0.5
    b = 0.7
    assert count(a,b,'*') == (0.5*0.7)

def test_valid_mult_negative_float():
    a = -0.5
    b = 0.7
    assert count(a,b,'*') == (-0.5*0.7)

def test_div_by_zero():
    a = random.randint(0,100000)
    b = 0
    assert count(a,b,'/') == "Division by zero!"
